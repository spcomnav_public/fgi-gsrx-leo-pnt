%FF LEO-PNT-UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tR = phaseLoopFilterO2aFLLO1(tR,ch)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set local variables
trackChannelData = tR.channel(ch);
loopCnt = trackChannelData.loopCnt;
PDIcarr = trackChannelData.PDIcarr;

% Calculate phase error from discriminator function
pllDiscr = trackChannelData.pllDiscr(loopCnt);
fllDiscr = trackChannelData.fllDiscr(loopCnt);
% Phase locked loop filter: Kaplan scheme using bilinear transform
% integrators
switch(trackChannelData.trackState)
    case 'STATE_PULL_IN'
        BWPLL = trackChannelData.pllNoiseBandwidthWide;
        BWFLL = trackChannelData.fllNoiseBandwidthWide;
    case 'STATE_COARSE_TRACKING'
        BWPLL = trackChannelData.pllNoiseBandwidthNarrow;
        BWFLL = trackChannelData.fllNoiseBandwidthNarrow;
    case 'STATE_FINE_TRACKING'
        BWPLL = trackChannelData.pllNoiseBandwidthVeryNarrow;
        BWFLL = trackChannelData.fllNoiseBandwidthVeryNarrow;
end

Wn = BWPLL/0.53;

phi_dd = Wn^2*pllDiscr;
phi_d = 1.414*Wn*pllDiscr;

WnFLL = BWFLL/0.25;

phi_d_FLL = WnFLL*fllDiscr;

trackChannelData.phaseDiscrFilt_deriv(loopCnt) = phi_dd;

IR1 = phi_dd*PDIcarr;
IR2 = IR1 + trackChannelData.prevIR2_PLL(loopCnt) + phi_d_FLL;
IR3 = 0.5*(IR2 + trackChannelData.prevIR2_PLL(loopCnt));
trackChannelData.prevIR2_PLL(loopCnt + 1) = IR2;

phi_d = phi_d + IR3;
trackChannelData.phaseDiscrFilt(loopCnt) = phi_d;

trackChannelData.prevIR5_PLL(loopCnt + 1) = 0.0;

% Copy updated local variables
tR.channel(ch) = trackChannelData;


