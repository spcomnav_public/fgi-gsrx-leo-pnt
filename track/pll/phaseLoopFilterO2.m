%FF LEO-PNT-UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tR = phaseLoopFilterO2(tR,ch)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set local variables
trackChannelData = tR.channel(ch);
loopCnt = trackChannelData.loopCnt;
PDIcarr = trackChannelData.PDIcarr;

% Calculate phase error from discriminator function
pllDiscr = trackChannelData.pllDiscr(loopCnt);
% Phase locked loop filter: Kaplan scheme using bilinear transform
% integrators
switch(trackChannelData.trackState)
    case 'STATE_PULL_IN'
        BWPLL = trackChannelData.pllNoiseBandwidthWide;
    case 'STATE_COARSE_TRACKING'
        BWPLL = trackChannelData.pllNoiseBandwidthNarrow;
    case 'STATE_FINE_TRACKING'
        BWPLL = trackChannelData.pllNoiseBandwidthVeryNarrow;
end

Wn = BWPLL/0.53;

phi_dd = Wn^2*pllDiscr;
phi_d = 1.414*Wn*pllDiscr;

trackChannelData.phaseDiscrFilt_deriv(loopCnt) = phi_dd;

IR1 = phi_dd*PDIcarr;
IR2 = IR1 + trackChannelData.prevIR2_PLL(loopCnt);
IR3 = 0.5*(IR2 + trackChannelData.prevIR2_PLL(loopCnt));
trackChannelData.prevIR2_PLL(loopCnt + 1) = IR2;

phi_d = phi_d + IR3;
trackChannelData.phaseDiscrFilt(loopCnt) = phi_d;

trackChannelData.prevIR5_PLL(loopCnt + 1) = 0.0;

% Copy updated local variables
tR.channel(ch) = trackChannelData;


