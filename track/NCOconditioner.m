%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FF LEO-PNT-UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tR = NCOconditioner(tR,ch)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copy updated local variables
trackChannelData = tR.channel(ch);
loopCnt = trackChannelData.loopCnt;
carrFreqBasis = trackChannelData.acquiredFreq;
codeFreqBasis = tR.codeFreqBasis;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CARRIER
totalCarrError = trackChannelData.phaseDiscrFilt(loopCnt);
trackChannelData.carrError(loopCnt) = totalCarrError;

% Calculate NCO feedback
carrNco = totalCarrError;

% Calculate carrier frequency
carrFreq = carrFreqBasis + totalCarrError;
trackChannelData.carrFreq(loopCnt) = carrFreq;

% Store values for next round -> USED IN OTHER PLACES
trackChannelData.prevCarrFreq = carrNco;
trackChannelData.prevCarrError = totalCarrError;

% Calculate doppler frequency
trackChannelData.doppler(loopCnt) = carrFreq - trackChannelData.intermediateFreq; %doppler = (IF frequency estimate during current loop of PLL) - (base IF freq)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CODE
% Calculate code error from discriminator function
codeError = trackChannelData.codeDiscrFilt(loopCnt);
trackChannelData.codeError = codeError;   

% Calculate NCO feedback
codeNco = codeError;

trackChannelData.codeNco(loopCnt) = codeNco;            

% Calculate code frequency
% if(trackChannelData.bInited)
%     trackChannelData.carrToCodeRatio = (tR.centerFrequency + trackChannelData.doppler(loopCnt-1))/trackChannelData.codeFreq(loopCnt-1);
% end
codeFreq = codeFreqBasis - codeNco + ( carrFreq - trackChannelData.intermediateFreq )/trackChannelData.carrToCodeRatio;
trackChannelData.codeFreq(loopCnt) = codeFreq;
trackChannelData.prevCodeFreq = codeFreq;

% Store values for next round -> NOT NEEDED
trackChannelData.prevCodeNco = codeNco;
trackChannelData.prevCodeError = codeError;   

%Compute accumulated phase
%blockSize = tR.codeLengthInChips/(tR.codeFreqBasis/tR.samplingFreq);
%blockSize = trackChannelData.blockSize(loopCnt); WRONG
%carrPhase = trackChannelData.prevAccumCarrPhase;
%time    = (1:blockSize) ./ tR.samplingFreq;
%trigarg = -(((carrFreq - (time -1/tR.samplingFreq)*trackChannelData.phaseDiscrFilt_deriv) * 2.0 * pi) .* time) + carrPhase;
%if(trackChannelData.bInited)
    %trackChannelData.accumCarrPhase(loopCnt) = trackChannelData.accumCarrPhase(loopCnt - 1) + (trigarg(blockSize) - carrPhase)/(2*pi);
    %trackChannelData.accumCarrPhase(loopCnt) = trackChannelData.accumCarrPhase(loopCnt - 1) -carrFreq*trackChannelData.Nc +trackChannelData.phaseDiscrFilt_deriv*trackChannelData.Nc^2;
%else
    %trackChannelData.accumCarrPhase(loopCnt) = trigarg(blockSize)/(2*pi);
    %trackChannelData.accumCarrPhase(loopCnt) = -carrFreq*trackChannelData.Nc +trackChannelData.phaseDiscrFilt_deriv*trackChannelData.Nc^2;
%end
%trackChannelData.prevAccumCarrPhase = rem(trigarg(blockSize), (2 * pi));
% if(trackChannelData.bInited)
%     p_jerk = polyfit((max(1,loopCnt - 200):loopCnt)*trackChannelData.Nc, trackChannelData.phaseDiscrFilt_deriv(max(1,loopCnt - 200):loopCnt),1);
% else
%     p_jerk = [0,0];
% end
trackChannelData.accumCarrPhase(loopCnt) = -carrFreq*trackChannelData.Nc -trackChannelData.phaseDiscrFilt_deriv(loopCnt)*trackChannelData.Nc^2; %+ p_jerk(1)*trackChannelData.Nc^3;

% Copy updated local variables
tR.channel(ch) = trackChannelData;

