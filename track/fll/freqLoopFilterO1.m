%FF LEO-PNT-UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tR = freqLoopFilterO1(tR,ch)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set local variables
trackChannelData = tR.channel(ch);
loopCnt = trackChannelData.loopCnt;

% Calculate phase error from discriminator function
fllDiscr = trackChannelData.fllDiscr(loopCnt);
% Phase locked loop filter: Kaplan scheme using bilinear transform
% integrators
switch(trackChannelData.trackState)
    case 'STATE_PULL_IN'
        BWFLL = trackChannelData.fllNoiseBandwidthWide;
    case 'STATE_COARSE_TRACKING'
        BWFLL = trackChannelData.fllNoiseBandwidthNarrow;
    case 'STATE_FINE_TRACKING'
        BWFLL = trackChannelData.fllNoiseBandwidthVeryNarrow;
end

WnFLL = BWFLL/0.25;

phi_d_FLL = WnFLL*fllDiscr;

trackChannelData.phaseDiscrFilt_deriv(loopCnt) = 0.0;

IR2 = trackChannelData.prevIR2_PLL(loopCnt) + phi_d_FLL;
IR3 = 0.5*(IR2 + trackChannelData.prevIR2_PLL(loopCnt));
trackChannelData.prevIR2_PLL(loopCnt + 1) = IR2;

phi_d = IR3;
trackChannelData.phaseDiscrFilt(loopCnt) = phi_d;

trackChannelData.prevIR5_PLL(loopCnt + 1) = 0.0;

% Copy updated local variables
tR.channel(ch) = trackChannelData;


