%FF LEO-PNT-UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tR = freqLoopFilterO2(tR,ch)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set local variables
trackChannelData = tR.channel(ch);
loopCnt = trackChannelData.loopCnt;
PDIcarr = trackChannelData.PDIcarr;

% Calculate phase error from discriminator function
fllDiscr = trackChannelData.fllDiscr(loopCnt);
% Phase locked loop filter: Kaplan scheme using bilinear transform
% integrators
switch(trackChannelData.trackState)
    case 'STATE_PULL_IN'
        BWFLL = trackChannelData.fllNoiseBandwidthWide;
    case 'STATE_COARSE_TRACKING'
        BWFLL = trackChannelData.fllNoiseBandwidthNarrow;
    case 'STATE_FINE_TRACKING'
        BWFLL = trackChannelData.fllNoiseBandwidthVeryNarrow;
end

WnFLL = BWFLL/0.53;

phi_dd = WnFLL^2*fllDiscr;
phi_d = 1.414*WnFLL*fllDiscr;

IR2 = trackChannelData.prevIR2_PLL(loopCnt) + phi_dd;
IR3 = 0.5*(IR2 + trackChannelData.prevIR2_PLL(loopCnt));
trackChannelData.prevIR2_PLL(loopCnt + 1) = IR2;

phi_dd = IR3;
trackChannelData.phaseDiscrFilt_deriv(loopCnt) = phi_dd;

IR4 = phi_dd*PDIcarr;
IR5 = IR4 + trackChannelData.prevIR5_PLL(loopCnt) + phi_d;
IR6 = 0.5*(IR5 + trackChannelData.prevIR5_PLL(loopCnt));
trackChannelData.prevIR5_PLL(loopCnt + 1) = IR5;

phi_d = IR6;
trackChannelData.phaseDiscrFilt(loopCnt) = phi_d;

% Copy updated local variables
tR.channel(ch) = trackChannelData;


