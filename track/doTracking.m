%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright 2015-2021 Finnish Geospatial Research Institute FGI, National
%% Land Survey of Finland. This file is part of FGI-GSRx software-defined
%% receiver. FGI-GSRx is a free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as published
%% by the Free Software Foundation, either version 3 of the License, or any
%% later version. FGI-GSRx software receiver is distributed in the hope
%% that it will be useful, but WITHOUT ANY WARRANTY, without even the
%% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%% See the GNU General Public License for more details. You should have
%% received a copy of the GNU General Public License along with FGI-GSRx
%% software-defined receiver. If not, please visit the following website 
%% for further information: https://www.gnu.org/licenses/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [trackResults]= doTracking(acqResults, allSettings)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function takes input of acquisition results and performs tracking.
%
% Inputs:
%   acqResults      - Results from signal acquisition for all signals
%   allSettings     - Receiver settings
%
% Outputs:
%   trackResults    - Results from signal tracking for all signals
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Start timer for tracking
trackStartTime = now;

% UI output
disp (['   Tracking started at ', datestr(trackStartTime)]); 

% Initialise tracking structure
trackResults = initTracking(acqResults, allSettings);  

% Let's loop over all enabled signals and open files for reading
for signalNr = 1:allSettings.sys.nrOfSignals

    % Extract block of parameters for one signal from settings
    signal = allSettings.sys.enabledSignals{signalNr};
    signalSettings = allSettings.(signal);
    
    % Open file for reading
    [fid, message] = fopen(signalSettings.rfFileName, 'rb');
    if (fid == -1)
       error('Failed to open data file for tracking!');
       return;
    else
       fidTemp{signalNr} = fid;
    end
end

t1=clock;

%for loopCnt =  1:allSettings.sys.msToProcess % Loop over all epochs
for gloopCnt =  1:floor(allSettings.sys.msToProcess*allSettings.sys.loopFactor) % Loop over all epochs
    for signalNr = 1:allSettings.sys.nrOfSignals % Loop over all signals
        signal = allSettings.sys.enabledSignals{signalNr};
        
        for channelNr = 1:trackResults.(signal).nrObs % Loop over all channels
%             loopCnt = trackResults.(signal).channel(channelNr).loopCnt + 1;
%             trackResults.(signal).channel(channelNr).loopCnt = loopCnt;
             % Set file pointer
            %trackResults.(signal).fid = fidTemp{signalNr};

            % Check epoch boundary
            %if(mod(loopCnt,trackResults.(signal).codeLengthInMs)==0)
            if(mod(gloopCnt,floor(trackResults.(signal).channel(channelNr).Nc*1000*allSettings.sys.loopFactor))==0)

                % Set file pointer
                trackResults.(signal).fid = fidTemp{signalNr};
                loopCnt = trackResults.(signal).channel(channelNr).loopCnt + 1;
                trackResults.(signal).channel(channelNr).loopCnt = loopCnt;

                %% FF LEO-PNT: Adding coherent integration
                trackResults.(signal).channel(channelNr).I_E(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).I_P(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).I_L(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).Q_E(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).Q_P(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).Q_L(loopCnt) = 0;     
                trackResults.(signal).channel(channelNr).I_E_E(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).Q_E_E(loopCnt) = 0;

                for cohInt = 1:floor(trackResults.(signal).channel(channelNr).Nc*1000/trackResults.(signal).codeLengthInMs)
                    % Correlate signal
                    trackResults.(signal) = GNSSCorrelation(trackResults.(signal),channelNr);
                    if((cohInt==1)&&(~trackResults.(signal).channel(channelNr).bInited)&&(loopCnt==1))
                        trackResults.(signal).channel(channelNr).bInited = true;
                        trackResults.(signal).channel(channelNr).prevCodeFreq = trackResults.(signal).codeFreqBasis + ( - trackResults.(signal).channel(channelNr).acquiredFreq - trackResults.(signal).channel(channelNr).intermediateFreq )/trackResults.(signal).channel(channelNr).carrToCodeRatio;
                    end
                end
                if((loopCnt==1)&&(trackResults.(signal).channel(channelNr).bInited))
                    trackResults.(signal).channel(channelNr).bInited = false;
                end

                %% FF LEO-PNT: Adding coherent integration
%                 trackResults.(signal).channel(channelNr).I_E(loopCnt) = trackResults.(signal).channel(channelNr).I_E(loopCnt)/cohInt;
%                 trackResults.(signal).channel(channelNr).I_P(loopCnt) = trackResults.(signal).channel(channelNr).I_P(loopCnt)/cohInt;
%                 trackResults.(signal).channel(channelNr).I_L(loopCnt) = trackResults.(signal).channel(channelNr).I_L(loopCnt)/cohInt;
%                 trackResults.(signal).channel(channelNr).Q_E(loopCnt) = trackResults.(signal).channel(channelNr).Q_E(loopCnt)/cohInt;
%                 trackResults.(signal).channel(channelNr).Q_P(loopCnt) = trackResults.(signal).channel(channelNr).Q_P(loopCnt)/cohInt;
%                 trackResults.(signal).channel(channelNr).Q_L(loopCnt) = trackResults.(signal).channel(channelNr).Q_L(loopCnt)/cohInt;     
%                 trackResults.(signal).channel(channelNr).I_E_E(loopCnt) = trackResults.(signal).channel(channelNr).I_E_E(loopCnt)/cohInt;
%                 trackResults.(signal).channel(channelNr).Q_E_E(loopCnt) = trackResults.(signal).channel(channelNr).Q_E_E(loopCnt)/cohInt;
    
                % Tracking of signal
                trackResults.(signal).channel(channelNr).fllDiscr(loopCnt) = 0;
                trackResults.(signal).channel(channelNr).pllDiscr(loopCnt) = 0;
                trackResults.(signal) = GNSSTracking(trackResults.(signal),channelNr); 
            end
        end
    end
    
    % UI function
    if (mod(gloopCnt, floor(100*allSettings.sys.loopFactor)) == 0)
        t2 = clock;
        time = etime(t2,t1);
        estimtime = floor(allSettings.sys.msToProcess*allSettings.sys.loopFactor)/gloopCnt*time;
        %showTrackStatus(trackResults,allSettings,loopCnt);
        msProcessed = gloopCnt/allSettings.sys.loopFactor;
        msLeftToProcess = allSettings.sys.msToProcess - gloopCnt/allSettings.sys.loopFactor;
        disp(['Ms Processed: ',int2str(msProcessed),' Ms Left: ',int2str(msLeftToProcess)]);
        disp(['Time processed: ',int2str(time),' Time left: ',int2str(estimtime-time)]);

     end    
end % Loop over all epochs

% Notify user tracking is over
disp(['   Tracking is over (elapsed time ', datestr(now - trackStartTime, 13), ')']) 


