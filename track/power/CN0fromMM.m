%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FF LEO-PNT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tR = CN0fromMM(tR,ch)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function for estimating CNO values using Moment Method
%
% Inputs:
%   tR             - Results from signal tracking for one signals
%   ch             - Channel index
%
% Outputs:
%   tR             - Results from signal tracking for one signals
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set local variables
trackChannelData = tR.channel(ch);
loopCnt = trackChannelData.loopCnt;
loopFactor = tR.loopFactor;

%intervalEpoch = trackChannelData.Nc*1000;
intervalEpoch = 1;
N = floor(trackChannelData.M*trackChannelData.K*loopFactor);

if(trackChannelData.bInited)
    iCount = max(1, loopCnt - N + 1):intervalEpoch:loopCnt;
    M2 = mean( trackChannelData.I_P(iCount).^2 + trackChannelData.Q_P(iCount).^2 );
    M4 = mean( (trackChannelData.I_P(iCount).^2 + trackChannelData.Q_P(iCount).^2).^2 );
    Pd = sqrt(2*M2^2 - M4);
    Pn = M2 - Pd;
    trackChannelData.CN0fromMM(loopCnt) = 10*log10((Pd/Pn)/trackChannelData.PDIcode);
    if loopCnt==2   
        % Fill up the first C/N0 estimate with the 2nd C/N0 estimate: just
        % to avoid putting zero for the first estimate
        trackChannelData.CN0fromMM(1) = 10*log10((Pd/Pn)/trackChannelData.PDIcode);   
    end
end

% Copy updated local variables
tR.channel(ch) = trackChannelData;