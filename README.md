# FGI-GSRx-LEO-PNT

Adaptation to LEO-PNT in acquisition and tracking engines by SPCOMNAV research group (IEEC/UAB).

Based on FGI-GSRx v1.0.0 release, available at https://github.com/nlsfi/FGI-GSRx


## License
GNU GPL-3.0 

