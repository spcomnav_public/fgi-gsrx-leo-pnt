%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright 2015-2021 Finnish Geospatial Research Institute FGI, National
%% Land Survey of Finland. This file is part of FGI-GSRx software-defined
%% receiver. FGI-GSRx is a free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as published
%% by the Free Software Foundation, either version 3 of the License, or any
%% later version. FGI-GSRx software receiver is distributed in the hope
%% that it will be useful, but WITHOUT ANY WARRANTY, without even the
%% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%% See the GNU General Public License for more details. You should have
%% received a copy of the GNU General Public License along with FGI-GSRx
%% software-defined receiver. If not, please visit the following website 
%% for further information: https://www.gnu.org/licenses/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [acqResults, allSettings] = doAcquisition(allSettings)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function acquires all enabled signals
%
% Inputs: 
%   allSettings         - Receiver settings

% Outputs:
%   acqResults          - Acquisition results
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialise acquisition structure
acqResults = initAcquisition(allSettings);

% Loop over all signals
for i = 1:allSettings.sys.nrOfSignals
    
    % Temporary variables
    signal = allSettings.sys.enabledSignals{i};
    
    skip_all = false;
    if strcmp(signal(1:2),'NB')
        WB_signal = signal(3:end);
        if any(strcmp(allSettings.sys.enabledSignals,WB_signal))
            skip_all = true; %NB acquisition will be performed during WB signal
        end
    end
    
    if(~skip_all)

        % Extract block of parameters for one signal from settings
        signalSettings = allSettings.(signal);

        if strcmp(signal(1:3),'leo')
            NB_signal = sprintf('NB%s',signal);
            if any(strcmp(allSettings.sys.enabledSignals,NB_signal))
                signalSettings = allSettings.(NB_signal); %Acquisition performed first with NB signal
            end
        end

        % Read RF Data
        %[pRfData,sampleCount] = getDataForAcquisition(signalSettings,100);
        msToRead = signalSettings.cohIntNumber*signalSettings.codeLengthMs*signalSettings.nonCohIntNumber;
        [pRfData,~] = getDataForAcquisition(signalSettings, msToRead);
        if(allSettings.sys.addNoise)
            pow_Noise = (allSettings.sys.ampSignal^2)*2*signalSettings.bandWidth/(10^(allSettings.sys.desiredCN0/10));
            pRfData = pRfData + sqrt(pow_Noise/2)*(randn(1,length(pRfData)) + 1i*randn(1,length(pRfData)));
        end
        % Execute acquisition for one signal
        acqResults.(signal) = acquireSignal(pRfData,signalSettings);

        if strcmp(signal(1:3),'leo')
            NB_signal = sprintf('NB%s',signal);
            if any(strcmp(allSettings.sys.enabledSignals,NB_signal))
                % Use NB results to Doppler-aid WB signal and get code delay
                acqResults.(NB_signal) = acqResults.(signal);
                [allSettings, acqResults] = adapt_WBleo_ACQ(signal, NB_signal, allSettings, acqResults);
                
                signalSettings = allSettings.(signal);

                % Read RF Data
                msToRead = signalSettings.cohIntNumber*signalSettings.codeLengthMs*signalSettings.nonCohIntNumber;
                [pRfData,~] = getDataForAcquisition(signalSettings, msToRead);
                if(allSettings.sys.addNoise)
                    pow_Noise = (allSettings.sys.ampSignal^2)*2*signalSettings.bandWidth/(10^(allSettings.sys.desiredCN0/10));
                    pRfData = pRfData + sqrt(pow_Noise/2)*(randn(1,length(pRfData)) + 1i*randn(1,length(pRfData)));
                end
                % Execute acquisition for one signal
                acqResults.(signal) = acquireSignal(pRfData,signalSettings);             
            end
        end

    end
    
end





