%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FF LEO-PNT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [allSettings_OUT, acqResults_OUT] = adapt_WBleo_ACQ(WB_signal, NB_signal, allSettings_IN, acqResults_IN)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Get PRNs acquired with NB signal
acqPRN = [];
acqDoppler = [];
acqDrift = [];
for k=1:acqResults_IN.(NB_signal).nrObs
    if acqResults_IN.(NB_signal).channel(k).bFound
        acqPRN = [acqPRN, allSettings_IN.(NB_signal).acqSatelliteList(k)];
        acqDoppler = [acqDoppler, acqResults_IN.(NB_signal).channel(k).carrFreq];
        acqDrift = [acqDrift, allSettings_IN.(NB_signal).aidedDopplerDrift(k)];
    end
end

allSettings_OUT = allSettings_IN;
allSettings_OUT.(WB_signal).acqSatelliteList = acqPRN;
allSettings_OUT.(WB_signal).numberOfChannels = length(acqPRN);
allSettings_OUT.(WB_signal).aidedDoppler = acqDoppler;
allSettings_OUT.(WB_signal).aidedNumFrqBins = ones(size(acqPRN));
allSettings_OUT.(WB_signal).aidedDopplerDrift = acqDrift;
allSettings_OUT.(WB_signal).freqRefFactor = 1;

for i = 1:allSettings_OUT.sys.nrOfSignals
    signal = allSettings_OUT.sys.enabledSignals{i};
    if(strcmp(signal, WB_signal))
        len = length(allSettings_OUT.(signal).acqSatelliteList);
        acqResults_OUT.(signal).signal = signal;
        acqResults_OUT.(signal).nrObs = len;
        acqResults_OUT.(signal).duration = 0;
        for k=1:len
            acqResults_OUT.(signal).channel(k).peakMetric = 0;
            acqResults_OUT.(signal).channel(k).peakValue = 0;
            acqResults_OUT.(signal).channel(k).variance = 0;
            acqResults_OUT.(signal).channel(k).baseline = 0;
            acqResults_OUT.(signal).channel(k).bFound = 0;
            acqResults_OUT.(signal).channel(k).carrFreq = 0;
            acqResults_OUT.(signal).channel(k).codePhase = 0;
            acqResults_OUT.(signal).channel(k).SvId = getSvId(signal,allSettings_OUT.(signal).acqSatelliteList(k));
            acqResults_OUT.(signal).channel(k).spec = zeros(1,allSettings_OUT.(signal).samplesPerCode);
        end
    else
        acqResults_OUT.(signal) = acqResults_IN.(signal);
    end
    
end

% Remove NB signal from the list
allSettings_OUT.sys.enabledSignals(ismember(allSettings_OUT.sys.enabledSignals,NB_signal)) = [];
allSettings_OUT.sys.nrOfSignals = allSettings_OUT.sys.nrOfSignals - 1;

end



