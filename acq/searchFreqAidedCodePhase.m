%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FF LEO-PNT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function searchResults = searchFreqAidedCodePhase( codeReplica, signalSettings, pRfData, centerFreq, numberOfFrqBins, freqStep, dopplerDrift )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aided Doppler search:
% from centerFreq - freqWindow
% to centerFreq - freqWindow + numberOfFrqBins*freqStep
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

apply_compensations = true; % Code Doppler compensation -> Required for high-rate codes and long integrations

% Set local variables
samplesPerCode = signalSettings.samplesPerCode;  % Number of samples per spreading code                    
cohIntNumber = signalSettings.cohIntNumber;
nonCohIntNumber = signalSettings.nonCohIntNumber; 
modCodeLengthInChips = signalSettings.codeLengthInChips * signalSettings.modulationFactor;

% Sampling period
ts = 1 / signalSettings.samplingFreq;

if ~apply_compensations
    codeFreq = signalSettings.codeFreqBasis;
    tc = 1/(codeFreq*signalSettings.modulationFactor);
    codeValueIndex = mod(ceil((ts * (1:samplesPerCode)) / tc), modCodeLengthInChips); 
    codeValueIndex(codeValueIndex == 0) = modCodeLengthInChips;
    codeValueIndex(end) = modCodeLengthInChips;
end

if (numberOfFrqBins == 0) % No Doppler aiding
    freqWindow = signalSettings.maxSearchFreq;
    numberOfFrqBins = floor(2 * freqWindow/freqStep + 1);
else
    freqWindow = floor(numberOfFrqBins/2)*freqStep;
end

% Allocate variables
frqBins = zeros(1, numberOfFrqBins); % Carrier frequencies of the frequency bins
searchResults = zeros(numberOfFrqBins,samplesPerCode); % Results structure
            
% Find phase points of the local carrier wave 
phasePoints = (0 : (cohIntNumber*samplesPerCode-1)) * 2 * pi * ts;  

% Loop over frequency bins
for frqBinIndex = 1:numberOfFrqBins    
     
    % Reset sum of all signals 
    sumNonCohAllSignals = zeros(1,samplesPerCode);
    
    % Loop over number of codes (pilot, data)
    for codeIndex = 1:size(codeReplica,1)  

        % Perform FFT on upsampled code
        if ~apply_compensations
            codeReplicaAdapted = codeReplica(codeIndex,codeValueIndex);
            codeFreqDom = conj(fft(codeReplicaAdapted));
        end

        % Calculate frequency of search bin
        frqBins(frqBinIndex) = centerFreq - ...
                               freqWindow + ...
                               freqStep * (frqBinIndex - 1);    
        
        % Generate local carrier frequency for bin
        if ~apply_compensations
            sigCarr = exp(-1i*frqBins(frqBinIndex) * phasePoints);   
        end

        % Reset sum of non coherent integration of one signal  
        sumNonCoh=zeros(1,samplesPerCode);
        
        % Reset variable for signal
        signal = zeros(nonCohIntNumber,cohIntNumber*samplesPerCode);
        
        % Loop over all non coherent rounds
        for nonCohIndex=1:nonCohIntNumber
            
            % Extract needed part of signal
            signal(nonCohIndex,:) = pRfData((nonCohIndex-1)*cohIntNumber*samplesPerCode+1:nonCohIndex*cohIntNumber*samplesPerCode);
            
            % Mix with carrier replica
            if apply_compensations
                time_vec = ((nonCohIndex - 1)*cohIntNumber*samplesPerCode + (0 : (cohIntNumber*samplesPerCode-1))) * ts;
                doppler_vec = frqBins(frqBinIndex) + dopplerDrift.*time_vec;
                sigCarr = exp(-1i*doppler_vec.* phasePoints);
            end
            IQ = sigCarr .* signal(nonCohIndex,:);
  
            % Reste sum of coherent integration
            sumCoh=zeros(1,samplesPerCode);
            
            % Coherent integration
            for cohIndex=1:cohIntNumber                                             
                
                cohIndex_vec = (cohIndex-1)*samplesPerCode+1:cohIndex*samplesPerCode;

                % FFT of signal mixed with carrier
                IQ_fft = fft(IQ(cohIndex_vec));

                % Code Doppler compensation
                if apply_compensations
                    codeFreq = signalSettings.codeFreqBasis + ( - doppler_vec(cohIndex_vec) )/(signalSettings.carrierFreq / signalSettings.codeFreqBasis);
                    tc = 1./(codeFreq*signalSettings.modulationFactor);
                    codeValueIndex = mod(ceil((ts * ((1 + ((nonCohIndex-1)*cohIntNumber + cohIndex - 1)*samplesPerCode):((nonCohIndex-1)*cohIntNumber + cohIndex)*samplesPerCode)) ./ tc), modCodeLengthInChips); 
                    codeValueIndex(codeValueIndex == 0) = modCodeLengthInChips;
                    codeReplicaAdapted = codeReplica(codeIndex,codeValueIndex);
                    codeFreqDom = conj(fft(codeReplicaAdapted));
                end

                % Inverse FFT of code times signal+carrier 
                sumCoh = sumCoh+ifft(IQ_fft.*codeFreqDom);

            end % End coherent integration
            
            % Non coherent integration. Accumulate results.
            sumNonCoh=sumNonCoh+abs(sumCoh);           
            
        end % End non coherent integration
        
        % Add results from all signals (pilot, data)
        sumNonCohAllSignals = sumNonCohAllSignals + sumNonCoh;
        
    end
    
    % Copy final result for current frequency bin
    searchResults(frqBinIndex,:)=sumNonCohAllSignals;    
end % End frequency bin search
